### Những kiến thức cần biết trước
Phần hướng dẫn được chuẩn bị sẵn giúp các bạn có thể chạy các mô hình phát hiện đối tượng (object detection) một cách đơn giản. Nhưng để có thể hiểu các khái niệm và có thể hoàn thành đồ án, các bạn cần biết (tự tìm hiểu) những vấn đề sau:

#### Phần lý thuyết:
Các bạn cần có những khái niệm cơ bản về máy học (machine learning). Các bạn có thể tham khảo các nguồn sau: 
Machine Learning Crash Course: https://developers.google.com/machine-learning/crash-course/
Một số bài giảng của khóa CS231n: Convolutional Neural Networks for Visual Recognition: http://cs231n.stanford.edu/syllabus.html
 
Để hiểu được các khái niệm cơ bản của bài toán Object Detection cùng với các thuật toán và các mô hình học sâu các bạn có thể tham khảo: 
Object Detection for Dummies: https://lilianweng.github.io/lil-log/tag/object-detection
Deep Learning for Objects and Scenes: http://deeplearning.csail.mit.edu/

#### Phần thực hành:
Trước khi chúng ta đi sâu hơn về những kỹ thuật “cao cấp" trong đồ án này. Chúng tôi đề nghị bạn phải có những kiến thức cơ bản nhất về lập trình Python, và xa hơn là Numpy, Tensorflow hay Pytorch. Kể cả khi bạn đã từng làm, từng học về các kiến thức này, chúng tôi cũng khuyến khích bạn xem qua lại một lần nữa. Nếu bạn chưa bao giờ tiếp cận đến chúng, thì đây sẽ là cơ hội cho bạn làm quen với môi trường notebook cũng như ngôn ngữ Python.

Trong bước đầu tiên này, chúng tôi đề nghị bạn xem kỹ các hướng dẫn  sau đây:


[Python - Numpy Tutorial notes]
http://cs231n.github.io/python-numpy-tutorial/

[Python Tutorial notebook]
http://cs231n.stanford.edu/notebooks/python_numpy_tutorial.ipynb
